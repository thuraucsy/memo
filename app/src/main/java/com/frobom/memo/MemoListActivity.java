package com.frobom.memo;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.frobom.memo.adapter.MemoViewAdapter;
import com.frobom.memo.data.Memo;
import com.frobom.memo.data.MemoDao;
import com.melnykov.fab.FloatingActionButton;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.InjectView;
import tr.xip.errorview.ErrorView;;


public class MemoListActivity extends AppCompatActivity implements AdapterView.OnClickListener,AdapterView.OnItemLongClickListener{
    private static final String LOG_TAG = MemoListActivity.class.getSimpleName();
    public static final String ROW_ID = "row_id";


    FloatingActionButton fab;
    private MemoDao mMemoDao;
    private ArrayList<Memo> mMemoList;
    private MemoViewAdapter mMemoViewAdapter;

    int catId = 0;
    ErrorView errorView;
    Memo memo;

   // @InjectView(R.id.memo_List)
    ListView mMemoListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memo_list);

        catId = getIntent().getIntExtra("id",0);

        mMemoListView = (ListView) findViewById(R.id.memo_List);
        errorView = (ErrorView) findViewById(R.id.error_view);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        mMemoDao = new MemoDao(this);
        mMemoList = new ArrayList<Memo>();
        setActionBar();

        mMemoViewAdapter = new MemoViewAdapter(this, mMemoList);
        mMemoListView.setAdapter(mMemoViewAdapter);
        mMemoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
               memo = mMemoList.get(position);

                Intent intent = new Intent(MemoListActivity.this, DetailAcivity.class)
                        .putExtra("memo",memo);
                intent.putExtra(ROW_ID,l);
                startActivity(intent);
            }
        });


        mMemoListView.setOnItemLongClickListener(this);

        fab.attachToListView(mMemoListView);
        fab.setOnClickListener(this);

    }


    private void setActionBar() {
        getSupportActionBar().setTitle("Memo List");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFEB3B")));
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        int id=item.getItemId();
        if(id==R.id.action_settings)
        {
            new MaterialDialog.Builder(this)
                    .title("Sort By..")
                    .items(R.array.sorting)
                    .itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog dialog, View view, int i, CharSequence text) {
                            switch (i) {
                                case 0:

                                    Collections.sort(mMemoList,new Comparator<Memo>() {
                                        @Override
                                        public int compare(Memo memo1, Memo memo2) {

                                           return memo1.getSubject().compareToIgnoreCase(memo2.getSubject());                                        }
                                    });

                                    mMemoViewAdapter.notifyDataSetChanged();



                                    break;

                                case 1:
                                    Collections.sort(mMemoList,new Comparator<Memo>() {
                                        @Override
                                        public int compare(Memo memo1, Memo memo2) {

                                            return memo2.getSubject().compareToIgnoreCase(memo1.getSubject());                                        }
                                    });

                                    mMemoViewAdapter.notifyDataSetChanged();


                                    break;
                                case 2:
                                    Collections.sort(mMemoList,new Comparator<Memo>() {
                                        @Override
                                        public int compare(Memo memo1, Memo memo2) {

                                            return memo1.getCreate_Date().compareToIgnoreCase(memo2.getCreate_Date());                                        }
                                    });

                                    mMemoViewAdapter.notifyDataSetChanged();
                                    break;
                                case 3:
                                    Collections.sort(mMemoList,new Comparator<Memo>() {
                                        @Override
                                        public int compare(Memo memo1, Memo memo2) {

                                            return memo2.getCreate_Date().compareToIgnoreCase(memo1.getCreate_Date());                                        }
                                    });

                                    mMemoViewAdapter.notifyDataSetChanged();
                                    break;

                                case 4:
                                    Collections.sort(mMemoList,new Comparator<Memo>() {
                                        @Override
                                        public int compare(Memo memo1, Memo memo2) {

                                            return memo2.getUpdate_Date().compareToIgnoreCase(memo1.getUpdate_Date());                                        }
                                    });

                                    mMemoViewAdapter.notifyDataSetChanged();
                                    break;




                                case 5:
                                    Collections.sort(mMemoList,new Comparator<Memo>() {
                                        @Override
                                        public int compare(Memo memo1, Memo memo2) {

                                            return memo1.getUpdate_Date().compareToIgnoreCase(memo2.getUpdate_Date());                                        }
                                    });

                                    mMemoViewAdapter.notifyDataSetChanged();


                                break;
                            }

                        }
                    })
                    .show();
        }
        return true;
    }

    private void getMemos() {
        mMemoList.clear();
       Log.v( mMemoDao.getAllById(catId).toString(),"Number Number");
        if(mMemoDao.getAllById(catId).size() == 0) {
            errorView.setVisibility(View.VISIBLE);
            errorView.setTitle("You have no Memo.");
            errorView.setSubtitle("");
            errorView.setTitleColor(Color.parseColor("#a3a3a3"));
            errorView.showRetryButton(false);
            errorView.setImageBitmap(null);
            Log.v("Ok Error","Error");
        }else {
            errorView.setVisibility(View.GONE);
            mMemoList.addAll(mMemoDao.getAllById(catId));
            Log.v("List",mMemoList.toString());
            mMemoViewAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {


        new MaterialDialog.Builder(this)
                .title("Choose action")
                .items(R.array.edit_memo)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int i, CharSequence text) {
                        switch (i) {
                            case 0:
                                memo = mMemoList.get(position);
                                Intent intent = new Intent(MemoListActivity.this, UpdateMemoActivity.class)
                                        .putExtra("memo",memo );


                                startActivity(intent);


                                break;
                            case 1:
                                deleteMemo(position);
                                break;
                        }
                    }
                })
                .show();
        return true;

    }

    private void deleteMemo(int position){
        try {

            mMemoDao.deleteMemo(mMemoList.get(position).id);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        getMemos();
        mMemoViewAdapter.notifyDataSetChanged();
    }
    @Override
    protected void onResume() {
        super.onResume();
        getMemos();
    }

    @Override
    public void onClick(View view) {
        Intent it = new Intent(getApplicationContext(),CreateMemoActivity.class);
        it.putExtra("id",catId);
        startActivity(it);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.memo_list, menu);
        return true;
    }





}
