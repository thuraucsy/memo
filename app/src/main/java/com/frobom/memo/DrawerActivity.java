package com.frobom.memo;

import android.os.Bundle;
import android.app.Fragment;

import it.neokree.materialnavigationdrawer.MaterialNavigationDrawer;

public class DrawerActivity extends MaterialNavigationDrawer{
    @Override
    public void init(Bundle bundle) {
        this.disableLearningPattern();
        this.setDrawerHeaderImage(R.drawable.memo);
        this.addSection(newSection("All Memos", R.drawable.all_memos, new MainActivity()));
        this.addSection(newSection("Category List", R.drawable.menu, new CategoryList()));
        this.addSection(newSection("Tag List", R.drawable.tag, new TagListActivity()));
        this.addSection(newSection("About", R.drawable.about, new AboutActivity()));


    }
}
