package com.frobom.memo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by DELL on 6/8/2015.
 */
public class Date {
    public static String getHttpDate()
    {
        Calendar calendar=Calendar.getInstance();
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/ MMM/ yyyy HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+06:30"));
        return simpleDateFormat.format(calendar.getTime());

    }
}
