package com.frobom.memo;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.frobom.memo.data.Category;
import com.frobom.memo.data.CategoryDao;
import com.frobom.memo.data.DatabaseHelper;
import com.frobom.memo.data.DatabaseManager;
import com.frobom.memo.data.Memo;
import com.frobom.memo.data.MemoDao;
import com.frobom.memo.data.Tag;
import com.frobom.memo.data.TagDao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.DatabaseConnection;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class DetailAcivity extends ActionBarActivity {

    TextView subjecttext;
    TextView bodytext;
    TextView tagtext;
    TextView categorytext;
    TextView createDate;
    TextView updateDate;


    private long rowID;
    MemoDao memoDao;
    Memo mMemo;
    TagDao tagDao;
    Tag tag = new Tag();
    List<Tag> tagList;
    ArrayList<Memo>mList;
    List<Memo> memos;
    CategoryDao categoryDao;
    List<Category> categoryList;
    int row_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_acivity);
        ButterKnife.inject(this);

        subjecttext = (TextView) findViewById(R.id.subjectText);
        bodytext = (TextView) findViewById(R.id.bodyText);
        tagtext = (TextView) findViewById(R.id.tagText);
        categorytext = (TextView) findViewById(R.id.categoryText);
        updateDate= (TextView) findViewById(R.id.update_date);
        createDate= (TextView) findViewById(R.id.create_date);


        mList=new ArrayList<Memo>();
        memoDao = new MemoDao(this);
        tagDao = new TagDao(this);
        categoryDao = new CategoryDao(this);
        mMemo= new Memo();
        mMemo = (Memo) getIntent().getSerializableExtra("memo");
        setActionBar();

        //get from db
        memos = memoDao.getMemoById(mMemo);
        tagList = tagDao.getTagByMemoId(mMemo);
        categoryList = categoryDao.getAllById(mMemo.getCatId());

        Bundle bundle=getIntent().getExtras();
        rowID=bundle.getLong(MemoListActivity.ROW_ID);
        if(memos.size() > 0 && tagList.size() >0 ){
            row_id=memos.get(0).getId();
            subjecttext.setText(memos.get(0).getSubject());
            bodytext.setText(memos.get(0).getBody());
            categorytext.setText(categoryList.get(0).getName());
            updateDate.setText(memos.get(0).getUpdate_Date());
            createDate.setText(memos.get(0).getCreate_Date());

            tagtext.setText("");
            Integer tagSize = tagList.size();
            String AllTag = "";
            for (int i = 0; i < tagSize; i++) {
                AllTag += (tagList.get(i).getName()) + " ";
            }
            tagtext.setText(AllTag);
        } else if (memos.size() > 0) {
            row_id=memos.get(0).getId();
            subjecttext.setText(memos.get(0).getSubject());
            bodytext.setText(memos.get(0).getBody());
            categorytext.setText(categoryList.get(0).getName());
            updateDate.setText(memos.get(0).getUpdate_Date());
            createDate.setText(memos.get(0).getCreate_Date());
            tagtext.setText("No Tag");
        }
        else {
            Toast.makeText(this, "Something wrong in detail:-(", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail_acivity, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        //get from db
        memos = memoDao.getMemoById(mMemo);
        tagList = tagDao.getTagByMemoId(mMemo);
        categoryDao = new CategoryDao(this);
        categoryList = categoryDao.getAllById(mMemo.getCatId());
        Log.e("categoryList", categoryList.toString());
        if(memos.size() > 0 && tagList.size() >0 ){
            row_id=memos.get(0).getId();
            subjecttext.setText(memos.get(0).getSubject());
            bodytext.setText(memos.get(0).getBody());
            categorytext.setText(categoryList.get(0).getName());
            updateDate.setText(memos.get(0).getUpdate_Date());
            createDate.setText(memos.get(0).getCreate_Date());

            tagtext.setText("");
            Integer tagSize = tagList.size();
            String AllTag = "";
            for (int i = 0; i < tagSize; i++) {
                AllTag += (tagList.get(i).getName()) + " ";
            }
            tagtext.setText(AllTag);
        } else if (memos.size() > 0) {
            row_id=memos.get(0).getId();
            subjecttext.setText(memos.get(0).getSubject());
            bodytext.setText(memos.get(0).getBody());
            categorytext.setText(categoryList.get(0).getName());
            updateDate.setText(memos.get(0).getUpdate_Date());
            createDate.setText(memos.get(0).getCreate_Date());
            tagtext.setText("No Tag");
        }
        else {
            Toast.makeText(this, "Something wrong in detail:-(", Toast.LENGTH_SHORT).show();
        }
//        finish();
//        startActivity(getIntent());
        //get from db
//        memos = memoDao.getMemoById(mMemo);
//        tagList = tagDao.getTagByMemoId(mMemo);
//        categoryList = categoryDao.getAllById(mMemo.getCatId());
//        Log.e("mMemo", mMemo.getBody());
//
//        if (memos.size() > 0) {
//            row_id=memos.get(0).getId();
//            subjecttext.setText(memos.get(0).getSubject());
//            bodytext.setText(memos.get(0).getBody());
//            updateDate.setText(memos.get(0).getUpdate_Date());
//            createDate.setText(memos.get(0).getCreate_Date());
//        }
//        if (tagList.size() > 0) {
//            tagtext.setText("");
//            Integer tagSize = tagList.size();
//            String AllTag = "";
//            for (int i = 0; i < tagSize; i++) {
//                AllTag += (tagList.get(i).getName()) + " ";
//            }
//            tagtext.setText(AllTag);
//        }
//        if (categoryList.size() > 0) {
//            categorytext.setText(categoryList.get(0).getName());
//        }
    }

    private void setActionBar(){
        getSupportActionBar().setTitle("Memo Detail");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFEB3B")));
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(DetailAcivity.this, UpdateMemoActivity.class)
                    .putExtra("memo", mMemo);
            intent.putExtra(MemoListActivity.ROW_ID,rowID);
            startActivity(intent);
            return true;
        }
        return true;
    }
}
