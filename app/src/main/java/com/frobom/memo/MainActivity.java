package com.frobom.memo;

import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.frobom.memo.adapter.MemoViewAdapter;
import com.frobom.memo.data.Memo;
import com.frobom.memo.data.MemoDao;
import com.melnykov.fab.FloatingActionButton;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.ButterKnife;
import butterknife.InjectView;
import tr.xip.errorview.ErrorView;


public class MainActivity extends Fragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    private MemoDao mMemoDao;
    private ArrayList<Memo> mMemoArrayList;
    private MemoViewAdapter mMemoViewAdapter;
    private Memo memo;

    @InjectView(R.id.error_view)
    ErrorView mErrorView;
    @InjectView(R.id.all_memo_List)
    ListView mAllListView;
    @InjectView(R.id.fab)
    FloatingActionButton fab;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_all_memo_list, container, false);
        ButterKnife.inject(this, v);
        setHasOptionsMenu(true);


        mMemoDao = new MemoDao(getActivity());
        mMemoArrayList = new ArrayList<>();


        try {
            getAllMemo();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        mMemoViewAdapter = new MemoViewAdapter(getActivity(),mMemoArrayList);

        mAllListView.setAdapter(mMemoViewAdapter);


        mAllListView.setOnItemClickListener(this);
        mAllListView.setOnItemLongClickListener(this);

        fab.attachToListView(mAllListView);
        fab.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent it = new Intent(getActivity(),CreateMemoActivity.class);
                startActivity(it);
            }
        });

        return v;
    }

    //CRUD Functions
    private void getAllMemo() throws SQLException {
        mMemoArrayList.clear();
        if(mMemoDao.getMemoAll().size()==0)
        {
            mErrorView.setVisibility(View.VISIBLE);
            mErrorView.setTitle("You have no Memo.");
            mErrorView.setSubtitle("");
            mErrorView.setTitleColor(Color.parseColor("#a3a3a3"));
            mErrorView.showRetryButton(false);
            mErrorView.setImageBitmap(null);
        }
        else
        {
            mMemoArrayList.addAll(mMemoDao.getMemoAll());
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.all_memo_sort,menu);

        super.onCreateOptionsMenu(menu, inflater);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        new MaterialDialog.Builder(getActivity())
                .title("Sort By..")
                .items(R.array.all_memo_sorting)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int i, CharSequence text) {
                        switch (i) {
                            case 0:

                                Collections.sort(mMemoArrayList, new Comparator<Memo>() {
                                    @Override
                                    public int compare(Memo memo1, Memo memo2) {

                                        return memo1.getSubject().compareToIgnoreCase(memo2.getSubject());
                                    }
                                });

                                mMemoViewAdapter.notifyDataSetChanged();



                                break;

                            case 1:
                                Collections.sort(mMemoArrayList,new Comparator<Memo>() {
                                    @Override
                                    public int compare(Memo memo1, Memo memo2) {

                                        return memo2.getSubject().compareToIgnoreCase(memo1.getSubject());                                        }
                                });

                                mMemoViewAdapter.notifyDataSetChanged();


                                break;
                            case 2:
                                Collections.sort(mMemoArrayList,new Comparator<Memo>() {
                                    @Override
                                    public int compare(Memo memo1, Memo memo2) {

                                        return memo1.getCreate_Date().compareToIgnoreCase(memo2.getCreate_Date());                                        }
                                });

                                mMemoViewAdapter.notifyDataSetChanged();
                                break;
                            case 3:
                                Collections.sort(mMemoArrayList,new Comparator<Memo>() {
                                    @Override
                                    public int compare(Memo memo1, Memo memo2) {

                                        return memo2.getCreate_Date().compareToIgnoreCase(memo1.getCreate_Date());                                        }
                                });

                                mMemoViewAdapter.notifyDataSetChanged();
                                break;

                            case 4:
                                Collections.sort(mMemoArrayList,new Comparator<Memo>() {
                                    @Override
                                    public int compare(Memo memo1, Memo memo2) {

                                        return memo2.getUpdate_Date().compareToIgnoreCase(memo1.getUpdate_Date());                                        }
                                });

                                mMemoViewAdapter.notifyDataSetChanged();

                                break;




                            case 5:
                                Collections.sort(mMemoArrayList,new Comparator<Memo>() {
                                    @Override
                                    public int compare(Memo memo1, Memo memo2) {

                                        return memo1.getUpdate_Date().compareToIgnoreCase(memo2.getUpdate_Date());                                        }
                                });

                                mMemoViewAdapter.notifyDataSetChanged();


                                break;
                        }

                    }
                })
                .show();

     return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {


        new MaterialDialog.Builder(getActivity())
                .title("Choose action")
                .items(R.array.edit_memo)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int i, CharSequence text) {
                        switch (i) {
                            case 0:
                                memo = mMemoArrayList.get(position);
                                Intent intent = new Intent(getActivity(), UpdateMemoActivity.class)
                                        .putExtra("memo",memo );


                                startActivity(intent);


                                break;
                            case 1:
                                try {
                                    delMemo(position);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                                break;
                        }
                    }
                })
                .show();
        return true;

    }
    private void delMemo(int position) throws SQLException {
        try {

            mMemoDao.deleteMemo(mMemoArrayList.get(position).id);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        getAllMemo();
        mMemoViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//        Intent it = new Intent(getActivity(), DetailAcivity.class);
//        it.putExtra("id", mMemoArrayList.get(i).id);
//        startActivity(it);
        memo = mMemoArrayList.get(i);
        Intent intent = new Intent(getActivity(), DetailAcivity.class)
                .putExtra("memo",memo);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            getAllMemo();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        mMemoViewAdapter.notifyDataSetChanged();
    }
}
