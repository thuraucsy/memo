package com.frobom.memo.data;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;


public class CategoryDao {
    public static final String EMPTY_REC = "just_empty";
    private Dao<Category, Integer> categoryDao;
    private ConnectionSource source;

    public CategoryDao(Context ctx) {
        DatabaseManager dbManager = new DatabaseManager();
        DatabaseHelper dbHelper = dbManager.getHelper(ctx);
        try{
            categoryDao = dbHelper.getGroupDao();
            source = dbHelper.getConnectionSource();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int create (Category category){
        try{
            return categoryDao.create(category);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Category> getAll() throws SQLException {
        return categoryDao.queryBuilder().query();
    }

    public List<Category> getAllById(int id) {
        try{
            QueryBuilder<Category, Integer> qb = categoryDao.queryBuilder();
            qb.where().eq("id", id);
            PreparedQuery<Category> pq = qb.prepare();
            return categoryDao.query(pq);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateCategory(Category category, int id) throws SQLException{
        categoryDao.createOrUpdate(category);
    }

    public void deleteCategory(int id) throws SQLException {
        categoryDao.deleteById(id);
    }
}
