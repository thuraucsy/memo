package com.frobom.memo.data;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

public class TagDao {

    private Dao<Tag, Integer> tagDao;
    private Dao<MemoTag, Integer> memoTagDao;
    private ConnectionSource source;

    public TagDao(Context c) {
        DatabaseManager dbManager = new DatabaseManager();
        DatabaseHelper dbHelper = dbManager.getHelper(c);
        try{
            tagDao = dbHelper.getTagDao();
            memoTagDao = dbHelper.getMemoTagDao();
            source = dbHelper.getConnectionSource();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public int create (Tag tag) {
        try {
            return tagDao.create(tag);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Tag> getAll() {
        try {
            return tagDao.queryBuilder().query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

//    public List<Tag> getTagById(Tag tag) {
//        QueryBuilder<Tag, Integer> queryBuilder = tagDao.queryBuilder();
//        try{
//            queryBuilder.where().eq("id", tag.getId());
//            PreparedQuery<Tag>preparedQuery = queryBuilder.prepare();
//            return tagDao.query(preparedQuery);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        return null;
//    }

    public List<Tag> getTagById(int i) {
        QueryBuilder<Tag, Integer> queryBuilder = tagDao.queryBuilder();
        try{
            queryBuilder.where().eq("id", i);
            PreparedQuery<Tag>preparedQuery = queryBuilder.prepare();
            return tagDao.query(preparedQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<Tag> getTagByMemoId(Memo mMemo) {

        int memoId = mMemo.getId();
        try {
            QueryBuilder<MemoTag, Integer> memoTagQueryBuilder = memoTagDao.queryBuilder();

            SelectArg nameSelectArg = new SelectArg();
            // this give memoTag memo_id =?
            memoTagQueryBuilder.where().eq("memo_id", nameSelectArg);
            nameSelectArg.setValue(mMemo.getId());
            QueryBuilder<Tag, Integer> tagQueryBuilder = tagDao.queryBuilder();
            return tagQueryBuilder.join(memoTagQueryBuilder).query();
//            return tagQueryBuilder.join(memoTagQueryBuilder).query();

//            queryBuilder.where().eq("memo_id", memoId);
//            queryBuilder.join(tagQueryBuilder);
//            PreparedQuery<MemoTag>preparedQuery = queryBuilder.prepare();
//            return tagDao.query(preparedQuery);
            //PreparedQuery<MemoTag>preparedQuery = queryBuilder.prepare();
//            return tagQueryBuilder.query();
//            memoTagDao.query(preparedQuery);
//            return queryBuilder.query();
//            return tagDao.queryRawValue("select * from tag, memotag");
//
//            Log.e("Memo Id ", String.valueOf(memoId));
//            memoTagQueryBuilder.where().eq(MemoTag.FOR_REF_MEMO, memoId);
//            memoTagQueryBuilder.join(tagQueryBuilder);
//            PreparedQuery<Tag> tagPreparedQuery = tagQueryBuilder.prepare();
//            return tagDao.query(tagPreparedQuery);


        } catch (SQLException e) {
            e.printStackTrace();
            Log.e("sql error", String.valueOf(e));
        }
        return null;
    }

    public void deleteTag(int id) throws SQLException {
        tagDao.deleteById(id);
    }

    public void updateTag(Tag tag, int id) throws SQLException {
        tagDao.createOrUpdate(tag);
    }

    public void updateTagByMemo(Tag tag) throws SQLException { tagDao.createOrUpdate(tag);}
}
