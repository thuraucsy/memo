package com.frobom.memo.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Database helper which creates and upgrades the database and provides the DAOs for the app.
 * 
 * @author kevingalligan
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

	/************************************************
	 * Suggested Copy/Paste code. Everything from here to the done block.
	 ************************************************/

	private static final String DATABASE_NAME = "memo.db";
	private static final int DATABASE_VERSION = 6;

	private Dao<Category, Integer> categoryDao=null;
    private Dao<Memo, Integer> memoDao=null;
    private Dao<Tag, Integer> tagDao=null;
    private Dao<MemoTag, Integer> memoTagDao=null;

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	/************************************************
	 * Suggested Copy/Paste Done
	 ************************************************/

	@Override
	public void onCreate(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource) {
		try {
			TableUtils.createTable(connectionSource, Category.class);
            TableUtils.createTable(connectionSource, Memo.class);
            TableUtils.createTable(connectionSource, Tag.class);
            TableUtils.createTable(connectionSource, MemoTag.class);
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "Unable to create datbases", e);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource, int oldVer, int newVer) {
		try {
			TableUtils.dropTable(connectionSource, Category.class, true);
            TableUtils.dropTable(connectionSource, Memo.class, true);
            TableUtils.dropTable(connectionSource, Tag.class, true);
            TableUtils.dropTable(connectionSource, MemoTag.class, true);
			onCreate(sqliteDatabase, connectionSource);
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "Unable to upgrade database from version " + oldVer + " to new "
					+ newVer, e);
		}
	}

	public Dao<Category, Integer> getGroupDao() throws SQLException {
		if (categoryDao == null) {
			categoryDao = getDao(Category.class);
		}
		return categoryDao;
	}

    public Dao<Memo, Integer> getMemoDao() throws SQLException {
        if (memoDao == null) {
            memoDao = getDao(Memo.class);
        }
        return memoDao;
    }

    public Dao<Tag, Integer> getTagDao() throws SQLException {
        if (tagDao == null) {
            tagDao = getDao(Tag.class);
        }
        return tagDao;
    }

    public Dao<MemoTag, Integer> getMemoTagDao() throws SQLException {
        if (memoTagDao == null) {
            memoTagDao = getDao(MemoTag.class);
        }
        return memoTagDao;
    }

    @Override
    public void close() {
        super.close();
        categoryDao = null;
        memoDao = null;
        tagDao = null;
        memoTagDao = null;
//        manufacturerDao = null;
//        locationDao = null;
//        colorDao = null;
    }




}
