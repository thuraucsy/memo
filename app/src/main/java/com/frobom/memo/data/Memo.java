package com.frobom.memo.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by DELL on 5/30/2015.
 */
@DatabaseTable
public class Memo implements Serializable{

    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField
    public int category_id;


    @DatabaseField
    public String subject;

    @DatabaseField
    public String body;

    @DatabaseField
    public String create_Date;

    @DatabaseField
    public String update_Date;


    public void setId(int id) {
        this.id = id;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setCatId(int category_id) {
        this.category_id = category_id;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setCreate_Date(String create_Date) {
        this.create_Date = create_Date;
    }

    public void setUpdate_Date(String update_Date) {
        this.update_Date = update_Date;
    }

    public int getId() {
        return id;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public int getCatId() {
        return category_id;
    }

    public String getCreate_Date() {
        return create_Date;
    }

    public String getUpdate_Date() {
        return update_Date;
    }

    @Override
    public String toString() {
        return subject == null ? "<None>" : subject;
    }

}
