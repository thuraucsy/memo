package com.frobom.memo.data;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by DELL on 5/30/2015.
 */
public class MemoDao{

    public static final String EMPTY_REC = "just_empty";
    private Dao<Memo, Integer> memoDao;
    private Dao<MemoTag, Integer> memoTagDao;
    private ConnectionSource source;

    public MemoDao(Context ctx) {
        DatabaseManager dbManager = new DatabaseManager();
        DatabaseHelper dbHelper = dbManager.getHelper(ctx);
        try{
            memoDao = dbHelper.getMemoDao();
            memoTagDao = dbHelper.getMemoTagDao();
            source = dbHelper.getConnectionSource();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int create (Memo memo){
        try{
            return memoDao.create(memo);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }


    public List<Memo> getMemoAll() throws SQLException{

            return memoDao.queryBuilder().query();
    }

    public List<Memo> getAllById(int id) {
        Log.e("memoId", String.valueOf(id));
        QueryBuilder<Memo,Integer>queryBuilder=memoDao.queryBuilder();
        try {
            queryBuilder.where().eq("category_id",id);
            PreparedQuery<Memo>preparedQuery=queryBuilder.prepare();
            Log.e("memoDao", String.valueOf(memoDao.query(preparedQuery)));
            return memoDao.query(preparedQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<Memo> getMemoById(Memo memo) {

        QueryBuilder<Memo,Integer>queryBuilder=memoDao.queryBuilder();
        try {
            queryBuilder.where().eq("id",memo.getId()).and().eq("category_id", memo.getCatId());
            PreparedQuery<Memo>preparedQuery=queryBuilder.prepare();

            return memoDao.query(preparedQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
    public void updateMemo(Memo memo) throws SQLException {
       memoDao.createOrUpdate(memo);

    }
    public void deleteMemo(int id) throws SQLException{

        memoDao.deleteById(id);
    }

    public List<Memo> getAllByTagId(int tagId) {
        try {
            QueryBuilder<MemoTag,Integer> mtQb = memoTagDao.queryBuilder();
            SelectArg nameSelectArg = new SelectArg();
            mtQb.where().eq(MemoTag.FOR_REF_TAG, nameSelectArg);
            nameSelectArg.setValue(tagId);
            QueryBuilder<Memo, Integer> mQb = memoDao.queryBuilder();
            return mQb.join(mtQb).query();
    } catch (SQLException e) {
            e.printStackTrace();
            Log.e("SQL error", String.valueOf(e));
        }
        return null;
    }

    public void deleteMemoByCat(Integer id) {
        SelectArg catId = new SelectArg(id);
        DeleteBuilder<Memo, Integer> qb = memoDao.deleteBuilder();
        try {
            qb.where().eq("category_id", catId);
            memoDao.delete(qb.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
