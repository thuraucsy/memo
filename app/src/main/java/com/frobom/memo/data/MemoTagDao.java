package com.frobom.memo.data;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

public class MemoTagDao {

    private Dao<MemoTag, Integer> memoTagDao;
    private ConnectionSource source;

    public MemoTagDao(Context c) {
        DatabaseManager dbManager = new DatabaseManager();
        DatabaseHelper dbHelper = dbManager.getHelper(c);
        try {
            memoTagDao = dbHelper.getMemoTagDao();
            source = dbHelper.getConnectionSource();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int create (MemoTag memoTag) {
        try {
            return memoTagDao.create(memoTag);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void updateMemoTag(MemoTag memoTag) throws SQLException {
        memoTagDao.createOrUpdate(memoTag);
    }

    public List<MemoTag> getByMemoId(Memo memo) {
        SelectArg memoId = new SelectArg(memo.getId());
        QueryBuilder<MemoTag, Integer> qb = memoTagDao.queryBuilder();
        try {
            qb.where().eq("memo_id", memoId);
            PreparedQuery<MemoTag> pq = qb.prepare();
            return memoTagDao.query(pq);
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        return new int[]{memoTag.getTag_id()};
        return null;
    }

    public List<MemoTag> getAll() throws SQLException {
        return memoTagDao.queryBuilder().query();
    }

    public void deleteMemoTag(int id) throws SQLException {
        SelectArg tagId = new SelectArg(id);
        DeleteBuilder<MemoTag, Integer> qb = memoTagDao.deleteBuilder();
        qb.where().eq(MemoTag.FOR_REF_TAG, tagId);
        try {
            memoTagDao.delete(qb.prepare());
//            qb.delete();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e("SQL error", String.valueOf(e));
        }
    }

    public void deleteMemoTagByMemo(Memo mMemo) {
        SelectArg memoId = new SelectArg(mMemo.getId());
        DeleteBuilder<MemoTag, Integer> qb = memoTagDao.deleteBuilder();
        try {
            qb.where().eq(MemoTag.FOR_REF_MEMO, memoId);
            memoTagDao.delete(qb.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
