package com.frobom.memo.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Category class object saved to the database
 */

@DatabaseTable
public class Category implements Serializable{

    private static final long serialVersionUID = -7874823823497497357L;

    @DatabaseField(generatedId = true)
    public Integer id;

    @DatabaseField
    public String name;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name == null ? "<None>" : name;
    }
}
