package com.frobom.memo.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable
public class MemoTag implements Serializable{

    public final static String FOR_REF_TAG =  "tag_id";
    public final static String FOR_REF_MEMO = "memo_id";

    public int memo_id;
    public int tag_id;

    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField(foreign = true, columnName = FOR_REF_TAG, foreignAutoRefresh = true)
    Tag tag;

    @DatabaseField(foreign = true, columnName = FOR_REF_MEMO, foreignAutoRefresh = true)
    Memo memo;

    public int getTag_id() {
        return tag.getId();
    }

    public void setTag_id(Tag tag) {
        //this.tag_id = tag_id;
//        this.tag.setId(tag_id);
        this.tag = tag;
        tag_id = tag.getId();
    }

    public int getMemo_id() {
        return memo.getId();
    }

    public void setMemo_id(Memo memo) {
//        this.memo.setId(memo_id);
        this.memo = memo;
        memo_id = memo.getId();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
