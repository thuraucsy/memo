package com.frobom.memo;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.frobom.memo.data.MemoTagDao;
import com.frobom.memo.data.Tag;
import com.frobom.memo.data.TagDao;
import com.melnykov.fab.FloatingActionButton;

import java.sql.SQLException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import tr.xip.errorview.ErrorView;

public class TagListActivity extends Fragment implements AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener {


    TagDao tagDao;
    MemoTagDao memoTagDao;
    ArrayList<Tag> tagList = null;
    @InjectView(R.id.error_view)
    ErrorView errorView;
    @InjectView(R.id.tagList)
    ListView listView;
    ArrayAdapter<Tag> adapter;
    @InjectView(R.id.fab)
    FloatingActionButton fab;

    EditText name;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tag_list,container,false);
        ButterKnife.inject(this,v);

        tagDao = new TagDao(getActivity());
        memoTagDao = new MemoTagDao(getActivity());

        tagList = new ArrayList<>();
        fillList();
        adapter = new ArrayAdapter<Tag>(getActivity(), R.layout.tag_row,R.id.tagName,tagList);
        listView.setAdapter(adapter);

        listView.setOnItemLongClickListener(this);
        listView.setOnItemClickListener(this);

        fab.attachToListView(listView);
        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                CreateTag.callMe(getActivity());
            }
        });

        return v;
    }

    private void fillList() {
        Log.e(TagListActivity.class.getName(), "Showing tag list");
        tagList.clear();
        if (tagDao.getAll().size() == 0) {
            errorView.setVisibility(View.VISIBLE);
            errorView.setTitle("You have no tag");
            errorView.setSubtitle("");
            errorView.setTitleColor(Color.parseColor("#a3a3a3"));
            errorView.showRetryButton(false);
            errorView.setImageBitmap(null);
        } else {
            errorView.setVisibility(View.GONE);
            tagList.addAll(tagDao.getAll());
            Log.e("Tag -> getAll", String.valueOf(tagDao.getAll()));
        }
    }

    public static void callMe(Context c) {
        c.startActivity(new Intent(c, TagListActivity.class));
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {
        new MaterialDialog.Builder(getActivity())
                .title("Choose action")
                .items(R.array.editOrDelete)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog materialDialog, View view, int i, CharSequence charSequence) {
                        switch (i) {
                            case 0:
                                updateTag(tagList.get(position), tagList.get(position).id);
                                break;
                            case 1:
                                try {
                                    deleteTag(position);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                        }
                    }
                }).show();
        return true;
    }

    private void deleteTag(int position) throws SQLException {
        int tagId = tagList.get(position).id;
        memoTagDao.deleteMemoTag(tagId);
        tagDao.deleteTag(tagId);
        fillList();
        adapter.notifyDataSetChanged();
    }

    private void updateTag(final Tag tag, final int id) {

        MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title("Update Tag")
                .customView(R.layout.update_tag, true)
                .positiveText("Accept")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        tag.name = name.getText().toString();
                        try {
                            tagDao.updateTag(tag, id);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        fillList();
                        adapter.notifyDataSetChanged();
                    }
                }).build();

        name = (EditText) dialog.getCustomView().findViewById(R.id.tagName);
        name.setText(tag.name);
        dialog.show();

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent it = new Intent(getActivity(), MemoByTagListActivity.class);
        it.putExtra("id", tagList.get(i).id);
        startActivity(it);
    }

    @Override
    public void onResume() {
        super.onResume();
        fillList();
        adapter.notifyDataSetChanged();
    }
}
