package com.frobom.memo;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.frobom.memo.adapter.MemoViewAdapter;
import com.frobom.memo.data.Memo;
import com.frobom.memo.data.MemoDao;
import com.frobom.memo.data.Tag;
import com.frobom.memo.data.TagDao;
import com.melnykov.fab.FloatingActionButton;

import java.sql.SQLException;
import java.util.ArrayList;

import tr.xip.errorview.ErrorView;

public class MemoByTagListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, View.OnClickListener {

    int tagId = 0;
    ListView listView;

    MemoDao memoDao;
    TagDao tagDao;
    ArrayList<Memo> memoList;
    ErrorView errorView;
    MemoViewAdapter mvAdapter;
    Memo memo;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memo_list_by_tag);

        tagId = getIntent().getIntExtra("id", 0);

        listView = (ListView) findViewById(R.id.memo_List);
        errorView = (ErrorView) findViewById(R.id.error_view);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        setActionBar();

        tagDao = new TagDao(this);
        memoDao = new MemoDao(this);
        memoList = new ArrayList<>();

        mvAdapter = new MemoViewAdapter(this, memoList);
        listView.setAdapter(mvAdapter);

        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);

        fab.attachToListView(listView);
        fab.setOnClickListener(this);

    }

    private void fillList() {
        memoList.clear();
        //Log.e("Memo from db", memoDao.getAllByTagId(tagId).toString());
        Log.e("tag id", String.valueOf(tagId));
        if (memoDao.getAllByTagId(tagId).size() == 0) {
            errorView.setVisibility(View.VISIBLE);
            errorView.setTitle("You have no Memo.");
            errorView.setSubtitle("");
            errorView.setTitleColor(Color.parseColor("#a3a3a3"));
            errorView.showRetryButton(false);
            errorView.setImageBitmap(null);
        } else {
            errorView.setVisibility(View.GONE);
            memoList.addAll(memoDao.getAllByTagId(tagId));
        }
        Log.e("MemoList from getAllByTagId", memoList.toString());
        mvAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        fillList();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        memo = memoList.get(i);
        Intent intent = new Intent(MemoByTagListActivity.this, DetailAcivity.class)
                .putExtra("memo",memo);
        intent.putExtra("row_id",l);
        startActivity(intent);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {
        new MaterialDialog.Builder(this)
                .title("Choose action")
                .items(R.array.edit_memo)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int i, CharSequence text) {
                        switch (i) {
                            case 0:
                                memo = memoList.get(position);
                                Intent intent = new Intent(MemoByTagListActivity.this, UpdateMemoActivity.class)
                                        .putExtra("memo",memo );


                                startActivity(intent);


                                break;
                            case 1:
                                deleteMemo(position);
                                break;
                        }
                    }
                })
                .show();
        return true;
    }

    private void deleteMemo(int position) {
        try {
            memoDao.deleteMemo(memoList.get(position).id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        fillList();
        mvAdapter.notifyDataSetChanged();
    }

    private void setActionBar() {
        getSupportActionBar().setTitle("Memo List By Tag");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFEB3B")));
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        Intent it = new Intent(getApplicationContext(),CreateMemoActivity.class);
        startActivity(it);
    }
}
