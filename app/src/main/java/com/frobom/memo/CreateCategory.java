package com.frobom.memo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.frobom.memo.data.Category;
import com.frobom.memo.data.CategoryDao;
import com.frobom.memo.data.DatabaseHelper;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class CreateCategory extends ActionBarActivity{

    @InjectView(R.id.categoryName)
    EditText categoryName;
    @InjectView(R.id.createCategory)
    Button createCategory;

    private CategoryDao categoryDao;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_category);
        ButterKnife.inject(this);
        setActionBar();
        categoryDao = new CategoryDao(this);
        createCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCat();
            }
        });

    }

    private void addCat() {
        Category category = new Category();
        category.name = categoryName.getText().toString();
        categoryDao.create(category);
        finish();
        Log.d("Category Insert done", category.toString());
    }

    private void setActionBar() {
        getSupportActionBar().setTitle("New Category");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFEB3B")));
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return true;
    }

    public static void callMe(Context c) {
        c.startActivity(new Intent(c, CreateCategory.class));
    }
}
