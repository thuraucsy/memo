package com.frobom.memo.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.frobom.memo.R;
import com.frobom.memo.data.Memo;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by DELL on 6/1/2015.
 */
public class MemoViewAdapter extends BaseAdapter {
    Context context;
    ArrayList<Memo> memoAL;

    public MemoViewAdapter(Context context,ArrayList<Memo> memoAL) {
        this.context = context;
        this.memoAL = memoAL;

    }

    @Override
    public int getCount() {
        return memoAL== null? 0 : memoAL.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        final ViewHolder vh;
        if(convertView == null){
            LayoutInflater inflater =  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.memo_list_row,viewGroup,false);

            vh = new ViewHolder(convertView);
            convertView.setTag(vh);
        }else{
            vh = (ViewHolder) convertView.getTag();
        }


        vh.memo_row.setText(memoAL.get(i).subject);

        return convertView;
    }

    static class ViewHolder{

        @InjectView(R.id.memo_row)
        TextView memo_row;

        public ViewHolder(View view){
            ButterKnife.inject(this, view);
        }
    }

}
