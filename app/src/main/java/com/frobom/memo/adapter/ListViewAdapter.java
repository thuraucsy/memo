package com.frobom.memo.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.frobom.memo.data.Category;

import java.util.ArrayList;

public class ListViewAdapter extends BaseAdapter{

    Context ctx;
    ArrayList<Category> al;


    public ListViewAdapter(Context ctx, ArrayList<Category>al) {
        this.ctx = ctx;
        this.al = al;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }
}
