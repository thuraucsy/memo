package com.frobom.memo;

import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.frobom.memo.data.Category;
import com.frobom.memo.data.CategoryDao;
import com.frobom.memo.data.Memo;
import com.frobom.memo.data.MemoDao;
import com.melnykov.fab.FloatingActionButton;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import tr.xip.errorview.ErrorView;


public class CategoryList extends Fragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    @InjectView(R.id.categoryList)
    ListView listView;
    ArrayList<Category> categoryList;
    CategoryDao categoryDao;
    MemoDao memoDao;
    ArrayAdapter<Category> adapter;
    EditText name;
    @InjectView(R.id.error_view)
    ErrorView errorView;
    @InjectView(R.id.fab)
    FloatingActionButton fab;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.category_list,container,false);
        ButterKnife.inject(this,v);

        setHasOptionsMenu(true);//for action menu


        categoryDao = new CategoryDao(getActivity());
        categoryList = new ArrayList<Category>();
        memoDao = new MemoDao(getActivity());
        // show list on create
        try {
            fillList();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        adapter = new ArrayAdapter<Category>(getActivity(), R.layout.category_row, R.id.categoryName, categoryList);
        listView.setAdapter(adapter);


        listView.setOnItemLongClickListener(this);
        listView.setOnItemClickListener(this);

        fab.attachToListView(listView);
        fab.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                  CreateCategory.callMe(getActivity());
            }
        });

        return v;

    }




    private void fillList() throws SQLException {
        Log.i(CategoryList.class.getName(), "Show List Again");
        categoryList.clear();
        if(categoryDao.getAll().size() == 0) {
            errorView.setVisibility(View.VISIBLE);
            errorView.setTitle("You have no category");
            errorView.setSubtitle("");
            errorView.setTitleColor(Color.parseColor("#a3a3a3"));
            errorView.showRetryButton(false);
            errorView.setImageBitmap(null);
        } else {
            errorView.setVisibility(View.GONE);
            categoryList.addAll(categoryDao.getAll());
        }
        Log.v("category",categoryList.toString());
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {


        new MaterialDialog.Builder(getActivity())
                .title("Choose action")
                .items(R.array.editOrDelete)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int i, CharSequence text) {
                        switch (i) {
                            case 0:
                               updateCategory(categoryList.get(position), categoryList.get(position).id);
                                break;
                            case 1:
                                try {
                                    deleteCategory(position);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                                break;
                        }
                    }
                })
                .show();
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Intent it=new Intent(getActivity(),MemoListActivity.class);
        it.putExtra("id", categoryList.get(i).id);
        startActivity(it);

    }

    //action menu
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.category_list,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==R.id.action_sort)
        {
            new MaterialDialog.Builder(getActivity())
                    .title("Sorted By..")
                    .items(R.array.category_sort)
                    .itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog dialog, View view, int i, CharSequence text) {
                            switch (i) {
                                case 0:
                                    Collections.sort(categoryList, new Comparator<Category>(){
                                        @Override
                                        public int compare(Category category1, Category category2) {

                                            return category1.getName().compareToIgnoreCase(category2.getName());
                                        }
                                    });

                                    adapter.notifyDataSetChanged();


                                    break;
                                case 1:
                                    Collections.sort(categoryList, new Comparator<Category>(){
                                        @Override
                                        public int compare(Category category1, Category category2) {

                                            return category2.getName().compareToIgnoreCase(category1.getName());
                                        }
                                    });

                                    adapter.notifyDataSetChanged();

                                    break;
                            }
                        }
                    })
                    .show();

        }
        return super.onOptionsItemSelected(item);

    }

    private void updateCategory(final Category category, final int id) {
        MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title("Update Category")
                .customView(R.layout.update_category, true)
                .positiveText("Accept")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        category.name = name.getText().toString();
                        try {
                            categoryDao.updateCategory(category, id);
                            fillList();
                            adapter.notifyDataSetChanged();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }).build();

        name = (EditText) dialog.getCustomView().findViewById(R.id.categoryName);
        name.setText(category.name);
        dialog.show();
    }

    private void deleteCategory(int position) throws SQLException {
        try{
            categoryDao.deleteCategory(categoryList.get(position).id);
            memoDao.deleteMemoByCat(categoryList.get(position).id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        fillList();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            fillList();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        adapter.notifyDataSetChanged();
    }
}
