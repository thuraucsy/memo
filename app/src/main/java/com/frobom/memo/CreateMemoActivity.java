package com.frobom.memo;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.frobom.memo.data.Category;
import com.frobom.memo.data.CategoryDao;
import com.frobom.memo.data.DatabaseHelper;
import com.frobom.memo.data.Memo;
import com.frobom.memo.data.MemoDao;
import com.frobom.memo.data.MemoTag;
import com.frobom.memo.data.MemoTagDao;
import com.frobom.memo.data.Tag;
import com.frobom.memo.data.TagDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class CreateMemoActivity extends ActionBarActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    @InjectView(R.id.subject)
    EditText subject;
    @InjectView(R.id.body)
    TextView body;
    @InjectView(R.id.tagName)
    TextView tagTextView;

    MemoDao memoDao;
    TagDao tagDao;
    Memo memo = new Memo();
    MemoTagDao memoTagDao;
    CategoryDao categoryDao;
    ArrayList<Tag> tagArrayList;
    List<Tag> tagList;
    List<Category> categoryList;
    Integer[] userChoseTagArrId = new Integer[0];
    @InjectView(R.id.spinner)
    Spinner categorySpinner;
    ArrayAdapter<String> spinnerArrayAdapter;
    int id=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_memo);
        ButterKnife.inject(this);
        setActionBar();

        memoDao=new MemoDao(this);
        tagDao = new TagDao(this);
        memoTagDao = new MemoTagDao(this);
        tagArrayList = new ArrayList<>();
        categoryDao = new CategoryDao(this);
        String userChoseCategory = null;

        // get intent value from previous page
        id = getIntent().getIntExtra("id",0);
        // Spinner setup
        try {
            categoryList = categoryDao.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String[]categoryName = new String[categoryList.size()];
            int i = 0;
            for (Category c: categoryList) {
                categoryName[i] = c.getName();
                Log.e("Category Name", c.getName());
                Log.e("Category Name Array", categoryName[i]);
                // save category name from db into a variable of id that was passed when user chose the category
                if (c.getId() == id ) {
                    userChoseCategory = c.getName();
                    Log.e("Same", c.getName());
                }
                i++;
            }
        spinnerArrayAdapter = new ArrayAdapter<String>(CreateMemoActivity.this, android.R.layout.simple_spinner_dropdown_item, categoryName);
        int spinnerPos = spinnerArrayAdapter.getPosition(userChoseCategory);
        categorySpinner.setAdapter(spinnerArrayAdapter);
        categorySpinner.setSelection(spinnerPos);
        categorySpinner.setOnItemSelectedListener(this);


        findViewById(R.id.memo_save).setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
//            MemoTag memoTag = new MemoTag();

            memo.category_id=id;
            memo.subject=subject.getText().toString();
            memo.body=body.getText().toString();
            memo.create_Date=Date.getHttpDate();
            memo.update_Date=Date.getHttpDate();
            Log.e("Created Date", memo.getCreate_Date());
            memoDao.create(memo);
            if ("".equals(memo.subject)) {
                Toast.makeText(getApplicationContext(),"You should fill the Subject", Toast.LENGTH_SHORT).show();
            } else if ("".equals(memo.body)) {
                Toast.makeText(getApplicationContext(),"You should fill the Body", Toast.LENGTH_SHORT).show();
            }

            // MemoTag Insert
            Tag tag;
            MemoTag memoTag = new MemoTag();
            memoTag.setMemo_id(memo);
            if (userChoseTagArrId.length != 0) {
                for (Integer i: userChoseTagArrId) {
                    //tag.name = tagNameArr[i];
                    tag = tagList.get(i);
                    Log.e("Tag List Name", tagList.get(i).getName());
//                    Log.e("TagNameArr[i]",tagNameArr[i]);
                    Log.e("Tag Name", tag.name);
                    memoTag.setTag_id(tag);
                    memoTagDao.create(memoTag);
                    Log.e("memoTag", String.valueOf(memoTag.getMemo_id()));
                    Log.e("memoTag", String.valueOf(memoTag.getTag_id()));
                    Log.e("memoTag", String.valueOf(memoTag.getId()));
                }
            }
            finish();

        Log.v("success",memo.toString());



    }

    public void clickDialog(View view) {
        String[] tagNameArr;
        tagList = tagDao.getAll();
        tagArrayList.addAll(tagDao.getAll());
        Log.e("tagListFromDb",tagArrayList.toString());

        // Get tagList name into variable first
        List<String> tagStringList = new ArrayList<>();
        // Fill tagList Name into tagStringList
        for (Tag t: tagList) {
            tagStringList.add(t.getName());
        }
        Log.e("tagStringList", String.valueOf(tagStringList));

        // Convert List to StringArray
        tagNameArr = tagStringList.toArray(new String[tagStringList.size()]);

        new MaterialDialog.Builder(this)
                .title("Tag List")
                .items(tagNameArr)
                .itemsCallbackMultiChoice(userChoseTagArrId, new MaterialDialog.ListCallbackMultiChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, Integer[] which, CharSequence[] text) {
                        for (Integer i : which) {
                            Log.e("Integer user chose", String.valueOf(i));
                        }
                        tagAdd(which);
                        fillTag(userChoseTagArrId);

                        return true;
                    }
                })
                .positiveText("choose")
                .show();

    }

    private void fillTag(Integer[] userChoseTagArrId) {
        String allTagNameOfMemo = "Tag: ";
        for (Integer i: userChoseTagArrId) {
            allTagNameOfMemo = allTagNameOfMemo + tagList.get(i).getName() + " ";
        }
        tagTextView.setText(allTagNameOfMemo);
    }

    private void tagAdd(Integer[] which) {
        int j = 0;
        userChoseTagArrId = new Integer[which.length];
        for (Integer i: which) {
            userChoseTagArrId[j] = i;
            Log.e("UserChoseTAgArrId", String.valueOf(userChoseTagArrId[j]));
            j++;
        }
    }




    private void setActionBar() {
        getSupportActionBar().setTitle("New Memo");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFEB3B")));
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Log.e("user Selected category", String.valueOf(adapterView.getItemAtPosition(i)));
        String userSelectedCatName = (String) adapterView.getItemAtPosition(i);
        for(Category c: categoryList) {
            if (c.getName() == userSelectedCatName) {
                id = c.getId();
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
