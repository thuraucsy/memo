package com.frobom.memo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.frobom.memo.data.Tag;
import com.frobom.memo.data.TagDao;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class CreateTag extends AppCompatActivity{

    @InjectView(R.id.tagName)
    EditText tagName;
    @InjectView(R.id.createTag)
    Button createTag;

    private TagDao tagDao;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_tag);
        ButterKnife.inject(this);

        setActionBar();
        tagDao = new TagDao(this);
        createTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTag();
            }
        });

    }

    private void addTag() {
        Tag tag = new Tag();
        tag.name = tagName.getText().toString();
        tagDao.create(tag);
        finish();
        Log.d("Tag Insert done", tag.toString());
    }

    private void setActionBar() {
        getSupportActionBar().setTitle("New Tag");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFEB3B")));
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return true;
    }


    public static void callMe(Context c) {
        c.startActivity(new Intent(c, CreateTag.class));
    }
}
