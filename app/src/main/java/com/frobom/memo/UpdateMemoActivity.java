package com.frobom.memo;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.frobom.memo.data.Category;
import com.frobom.memo.data.CategoryDao;
import com.frobom.memo.data.Memo;
import com.frobom.memo.data.MemoDao;
import com.frobom.memo.data.MemoTag;
import com.frobom.memo.data.MemoTagDao;
import com.frobom.memo.data.Tag;
import com.frobom.memo.data.TagDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class UpdateMemoActivity extends ActionBarActivity implements AdapterView.OnItemSelectedListener {

    @InjectView(R.id.subject)
    EditText subject;
    @InjectView(R.id.body)
    TextView body;
    @InjectView(R.id.tagName)
    TextView tagTextView;
    @InjectView(R.id.spinner)
    Spinner categorySpinner;
    ArrayAdapter<String> spinnerArrayAdapter;

    MemoDao memoDao;
    Memo mMemo;
    MemoTag memoTag;
    TagDao tagDao;
    TagDao updateTagDao;
    MemoTagDao memoTagDao;
    CategoryDao categoryDao;
    List<MemoTag> memoTagList;
    Tag tag;
    List<Tag> tagList;
    List<Tag> tagListByMemo;
    List<Category> categoryList;
    ArrayList<Tag> tagArrayList;
    Integer[] userChoseTagArrId;
    Button memo_update;
    String userChoseCategory = null;
    int tagId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_memo);
        ButterKnife.inject(this);

        memo_update= (Button) findViewById(R.id.memo_update);
        memoDao = new MemoDao(this);
        tagDao = new TagDao(this);
        updateTagDao = new TagDao(this);
        memoTagDao = new MemoTagDao(this);
        memoDao=new MemoDao(this);
        categoryDao = new CategoryDao(this);
        tagArrayList = new ArrayList<>();
        mMemo= new Memo();
        memoTag = new MemoTag();
        mMemo = (Memo) getIntent().getSerializableExtra("memo");
        tag = new Tag();


        initialize();
        List<Memo> memos = memoDao.getMemoById(mMemo);
        Log.e("query for all of tag", String.valueOf(tagDao.getTagByMemoId(mMemo)));
//        List<Tag> tag = tagDao.getTagByMemoId(mMemo);
        tagListByMemo = tagDao.getTagByMemoId(mMemo);
        List<Tag> tagList = tagDao.getAll();
        Log.e("tag from getTagByMemoId", tagListByMemo.toString());
        setActionBar();


        if(memos.size() > 0 && tagListByMemo.size() > 0){
            subject.setText(memos.get(0).getSubject());
            body.setText(memos.get(0).getBody());

            fillTag(tagListByMemo);
            //tagText.setText(tagList.get(0).getName());
            //get tagId for update
            //tagId = tagList.get(0).getId();
            //tagText.setText(tag.get(0).getName());

            // Fill into userChoseTagArrId from memoTag table
            userChoseTagArrId = new Integer[tagListByMemo.size()]; // set the size of array that user can chose from db
            memoTagList= memoTagDao.getByMemoId(mMemo);
            Log.e("memoTag usr chose tag id", String.valueOf(memoTagList.get(0).getId()));

            int j = 0;
            int k =0;
            for (Tag t: tagList ) {
                Log.e("Tag Id", String.valueOf(t.getId()));
                for (MemoTag mt: memoTagList) {
                    Log.e("MemoTag Id", String.valueOf(mt.getTag_id()));
                    if (t.getId() == mt.getTag_id()){
                        userChoseTagArrId[j] = k;
                        Log.e("Same", String.valueOf(k));
                        Log.e("j", String.valueOf(j));
                        Log.e("tag name", t.getName());
                        j++;
                    }
                }
                k++;
            }
            //Log.e("user CHose tag arr id", String.valueOf(userChoseTagArrId[0]));

        } else if(memos.size() > 0) {
            subject.setText(memos.get(0).getSubject());
            body.setText(memos.get(0).getBody());
        } else {
            Toast.makeText(this, "Something wrongs in update memo :-(", Toast.LENGTH_SHORT).show();
        }

        // Spinner setup
        try {
            categoryList = categoryDao.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String[]categoryName = new String[categoryList.size()];
        int i = 0;
        for (Category c: categoryList) {
            categoryName[i] = c.getName();
            Log.e("Category Name", c.getName());
            Log.e("Category Name Array", categoryName[i]);
            // save category name from db into a variable of id that was passed when user chose the category
            if (c.getId() == mMemo.getCatId() ) {
                userChoseCategory = c.getName();
                Log.e("Same", c.getName());
            }
            i++;
        }
        spinnerArrayAdapter = new ArrayAdapter<String>(UpdateMemoActivity.this, android.R.layout.simple_spinner_dropdown_item, categoryName);
        int spinnerPos = spinnerArrayAdapter.
                getPosition(userChoseCategory);
        categorySpinner.setAdapter(spinnerArrayAdapter);
        categorySpinner.setSelection(spinnerPos);
        categorySpinner.setOnItemSelectedListener(this);


        findViewById(R.id.memo_update).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                updateMemo();
                finish();
            }
        });
   }

    private void fillTag(List<Tag> tagListByMemo) {
        String allTagNameOfMemo = "Tag: ";
        for (Tag t: tagListByMemo) {
            allTagNameOfMemo = allTagNameOfMemo + t.getName() + " ";
        }
        tagTextView.setText(allTagNameOfMemo);
    }

    //initialize context
    private void initialize(){
        subject.setText(mMemo.subject);
        body.setText(mMemo.body);
        //tagTextView.setText(bymemo);
        //tagText.setText(tag.name);


    }

    private  void updateMemo()
    {
            mMemo.category_id = mMemo.getCatId();
            mMemo.subject = subject.getText().toString();
            mMemo.body = body.getText().toString();
            mMemo.update_Date=Date.getHttpDate();


            try {
                memoDao.updateMemo(mMemo);
                finish();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        Log.v("Update success", mMemo.toString());
    }

    public void clickDialog(View view) {
        String[] tagNameArr;
        tagList = tagDao.getAll();
        tagArrayList.addAll(tagDao.getAll());
        Log.e("tagListFromDb",tagArrayList.toString());

        // Get tagList name into variable first
        List<String> tagStringList = new ArrayList<>();
        // Fill tagList Name into tagStringList
        for (Tag t: tagList) {
            tagStringList.add(t.getName());
        }
        Log.e("tagStringList", String.valueOf(tagStringList));

        // Convert List to StringArray
        tagNameArr = tagStringList.toArray(new String[tagStringList.size()]);

        new MaterialDialog.Builder(this)
                .title("Tag List")
                .items(tagNameArr)
                .itemsCallbackMultiChoice(userChoseTagArrId, new MaterialDialog.ListCallbackMultiChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, Integer[] which, CharSequence[] text) {
                        for (Integer i : which) {
                            Log.e("Integer user chose", String.valueOf(i));
                        }
                        try {
                            tagAdd(which);
                            tagListByMemo = tagDao.getTagByMemoId(mMemo);
                            fillTag(tagListByMemo);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        return true;
                    }
                })
                .positiveText("choose")
                .show();

    }

    private void tagAdd(Integer[] which) throws SQLException {
        int j = 0;
        userChoseTagArrId = new Integer[which.length];
        for (Integer i: which) {
            userChoseTagArrId[j] = i;
            Log.e("UserChoseTAgArrId", String.valueOf(userChoseTagArrId[j]));
            j++;
        }

        // MemoTag Delete first
        memoTagDao.deleteMemoTagByMemo(mMemo);
        // MemoTag Insert
        Tag tag;
        memoTag.setMemo_id(mMemo);
        if (userChoseTagArrId.length != 0) {
            for (Integer i: userChoseTagArrId) {

                    // Set memoTag Id for update memo
                    if (memoTagList != null) {
                        for (MemoTag mt: memoTagList){
                            // user chose edit tag as it was, mt is from database
                            if(mt.getTag_id() == tagList.get(i).getId()) {
                                memoTag.setId(mt.getId());
                                Log.e("Memo Tag Id", String.valueOf(mt.getId()));
                                Log.e("database selected", String.valueOf(mt.getTag_id()));
                                Log.e("now user select", String.valueOf(tagList.get(i).getId()));
                            }
                        }
                    }
                    tag = tagList.get(i);
                    Log.e("tag", tag.getName());
                    memoTag.setTag_id(tag);
                    memoTagDao.updateMemoTag(memoTag);
                    memoTag.setId(-1); // very important step, if not, tag get only update the last one

            }
        } else {
            memoTagDao.deleteMemoTagByMemo(mMemo);
        }
    }

    private void setActionBar() {
        getSupportActionBar().setTitle("Update Category");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFEB3B")));
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String userSelectedCatName = (String) adapterView.getItemAtPosition(i);
        for(Category c: categoryList) {
            if (c.getName() == userSelectedCatName) {
                mMemo.category_id =c.getId();
                Log.e("c id", String.valueOf(c.getId()));
                Log.e("mMemo", String.valueOf(mMemo.getCatId()));
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
