package com.frobom.memo;

import android.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.frobom.memo.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.psdev.licensesdialog.LicensesDialog;
import de.psdev.licensesdialog.licenses.ApacheSoftwareLicense20;
import de.psdev.licensesdialog.licenses.MITLicense;
import de.psdev.licensesdialog.model.Notice;
import de.psdev.licensesdialog.model.Notices;




public class AboutActivity extends Fragment {

    @InjectView(R.id.txt_osl)
    TextView osl;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_about,container,false);

        ButterKnife.inject(this,v);
        osl.setClickable(true);
        oslColor(osl);
        osl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLicence();
            }
        });
        return v;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }

    void  showLicence()
    {

        final Notices notices = new Notices();
        notices.addNotice(new Notice("Material Navigation Drawer", "https://github.com/neokree/MaterialNavigationDrawer", "nerokee", new ApacheSoftwareLicense20()));
        notices.addNotice(new Notice("Floating Action Button", "https://github.com/makovkastar/FloatingActionButton", "makovkastar", new MITLicense()));
        notices.addNotice(new Notice("ErrorView","https://github.com/xiprox/ErrorView","xiprox",new ApacheSoftwareLicense20()));
        notices.addNotice(new Notice("ButterKnife","https://github.com/JakeWharton/butterknife","Jake Wharton",new ApacheSoftwareLicense20()));
        notices.addNotice(new Notice("OrmLite","","",new ApacheSoftwareLicense20()));
        notices.addNotice(new Notice("Material-Dialogs","https://github.com/afollestad/material-dialogs","afollestad",new ApacheSoftwareLicense20()));
        new LicensesDialog.Builder(getActivity()).setNotices(notices).setIncludeOwnLicense(true).setTitle("Open Source Licences").build().show();



    }

    private void oslColor(final TextView textView)
    {
        textView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction())
                {
                    case MotionEvent.ACTION_DOWN:
                        textView.setTextColor(getResources().getColor(R.color.primary));
                        break;
                    case MotionEvent.ACTION_UP:
                        textView.setTextColor(getResources().getColor(R.color.blue));
                        break;

                }
                return false;
            }
        });
    }


}
